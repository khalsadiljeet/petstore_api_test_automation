package tests;


import java.io.IOException;
import org.apache.http.HttpStatus;
import org.testng.AssertJUnit;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.helpers.StoreService;
import com.model.StoreOrder;
import com.utils.ConfigManager;
import com.utils.JsonUtility;
import io.restassured.response.Response;

/*This class file contains test cases related to Store API*/
@Listeners(com.utils.ReportUtility.class)
public class StoreAPITest {
	
    StoreService storeService;
    JsonUtility jsonUtility;
    ObjectMapper mapper;
    
    @BeforeClass
    public void init(){
    	storeService =new StoreService();
    	jsonUtility = new JsonUtility();
    	mapper = new ObjectMapper();
    }
    
	/*
	 * This test case performs POST call to place a pet order & then verify same
	 * purchase order details through GET call
	 */
    @Test(priority=1)
    public void placePetOrderAndVerifyPurchase() throws IOException {
    	
    	String postRequestPayload;
    	
    	// Fetching jsonpath from properties file
    	String order_id_jsonpath=ConfigManager.getInstance().getString("order_id");
    	String order_pet_id_jsonpath=ConfigManager.getInstance().getString("order_pet_id");
    	String order_quantity_jsonpath=ConfigManager.getInstance().getString("order_quantity");
    	String order_shipdate_jsonpath=ConfigManager.getInstance().getString("order_shipdate");
    	String order_status_jsonpath=ConfigManager.getInstance().getString("order_status");
    	String order_complete_jsonpath=ConfigManager.getInstance().getString("order_complete");
    	
    	// Fetching request payload from testData folder
    	postRequestPayload = jsonUtility.readFileAsString("resources/testData/store_order_pet_payload.json");
    	
    	// Fetching individual field values from request payload
    	String request_id = jsonUtility.getJsonPathFieldValue(postRequestPayload, order_id_jsonpath).toString();
    	String request_petid = jsonUtility.getJsonPathFieldValue(postRequestPayload, order_pet_id_jsonpath).toString();
    	String request_quantity = jsonUtility.getJsonPathFieldValue(postRequestPayload, order_quantity_jsonpath).toString();
    	String request_shipdate = jsonUtility.getJsonPathFieldValue(postRequestPayload, order_shipdate_jsonpath).toString();
    	String request_status = jsonUtility.getJsonPathFieldValue(postRequestPayload, order_status_jsonpath).toString();
    	String request_complete = jsonUtility.getJsonPathFieldValue(postRequestPayload, order_complete_jsonpath).toString();
    	
    	// POST call to place an order for pet
    	Response postResponse = storeService.storeOrderPet(postRequestPayload);
    	AssertJUnit.assertEquals(postResponse.getStatusCode(), HttpStatus.SC_OK);
    	
    	// GET call to verify purchase order placed in above step
    	Response getResponse = storeService.getStoreOrder(Integer.valueOf(request_id));
    	AssertJUnit.assertEquals(getResponse.getStatusCode(), HttpStatus.SC_OK);
    	
    	// Mapping GET call response to POJO
    	StoreOrder storeOrderObject = mapper.readValue(getResponse.getBody().asString(), StoreOrder.class);
    	
    	// Validating details between GET call response & POST call's request payload
    	AssertJUnit.assertEquals(String.valueOf(storeOrderObject.getId()), request_id);
    	AssertJUnit.assertEquals(String.valueOf(storeOrderObject.getPetId()), request_petid);
    	AssertJUnit.assertEquals(String.valueOf(storeOrderObject.getQuantity()), request_quantity);
    	AssertJUnit.assertEquals(storeOrderObject.getShipDate().split("T")[0], request_shipdate.split("T")[0]);
    	AssertJUnit.assertEquals(storeOrderObject.getStatus(), request_status);
    	AssertJUnit.assertEquals(storeOrderObject.getComplete().toString(), request_complete);
    	
    }
    
	/*
	 * This test case deletes the order at store & then verifies whether that order
	 * actually got deleted or not through GET call
	 */
    
    public void testDeleteOrder() throws IOException {
    	
    	String requestPayload;
    	
    	// Fetching request payload from testData folder
    	requestPayload = jsonUtility.readFileAsString("resources/testData/store_order_pet_payload.json");
    	
    	// Fetching jsonpath from properties file
    	String order_id_jsonpath=ConfigManager.getInstance().getString("order_id");
    	
    	// Fetching Order id value from request payload
    	String request_id = jsonUtility.getJsonPathFieldValue(requestPayload, order_id_jsonpath).toString();
    	
    	// DELETE call to delete the above placed Order
    	Response postResponse = storeService.deleteStoreOrder(Integer.valueOf(request_id));
    	AssertJUnit.assertEquals(postResponse.getStatusCode(), HttpStatus.SC_OK);
    	
    	// GET call to verify whether Order actually deleted or not
    	Response getResponse = storeService.getStoreOrder(Integer.valueOf(request_id));
    	AssertJUnit.assertEquals(getResponse.getStatusCode(), HttpStatus.SC_NOT_FOUND);
    }
    
 
}
