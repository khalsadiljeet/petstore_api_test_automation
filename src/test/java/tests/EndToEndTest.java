package tests;


import java.io.IOException;

import org.apache.http.HttpStatus;
import org.testng.AssertJUnit;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.helpers.PetService;
import com.helpers.StoreService;
import com.model.StoreOrder;
import com.utils.ConfigManager;
import com.utils.JsonUtility;
import io.restassured.response.Response;

/*This class file contains end to end test case*/
@Listeners(com.utils.ReportUtility.class)
public class EndToEndTest  {
	
    PetService petService;
    StoreService storeService;
    JsonUtility jsonUtility;
    ObjectMapper mapper;
    
    @BeforeClass
    public void init(){
    	petService =new PetService();
    	storeService =new StoreService();
    	jsonUtility = new JsonUtility();
    	mapper = new ObjectMapper();
    }
    
	/*
	 * This is end to end test case performs POST call to add a pet, place an order
	 * for this pet & then verifies purchase order through GET call 
	 */
	
    @Test(priority=1)
    public void addPetAndPlaceItsOrder() throws IOException {
    	
    	String addPetRequestPayload, petOrderRequestPayload;
    	
    	// Fetching request payload from testData folder
    	addPetRequestPayload = jsonUtility.readFileAsString("resources/testData/pet_payload.json");
    	
    	// POST call to add a pet to the store
    	Response addPetResponse = petService.addPetToStore(addPetRequestPayload);
    	AssertJUnit.assertEquals(addPetResponse.getStatusCode(), HttpStatus.SC_OK);
    	
    	// Fetching request payload from testData folder
    	petOrderRequestPayload = jsonUtility.readFileAsString("resources/testData/store_order_pet_payload.json");
    	
    	// Fetching jsonpath from properties file
    	String order_id_jsonpath=ConfigManager.getInstance().getString("order_id");
    	String order_pet_id_jsonpath=ConfigManager.getInstance().getString("order_pet_id");
    	String order_quantity_jsonpath=ConfigManager.getInstance().getString("order_quantity");
    	String order_shipdate_jsonpath=ConfigManager.getInstance().getString("order_shipdate");
    	String order_status_jsonpath=ConfigManager.getInstance().getString("order_status");
    	String order_complete_jsonpath=ConfigManager.getInstance().getString("order_complete");
    	
    	// Fetching individual field values from request payload
    	String request_order_id = jsonUtility.getJsonPathFieldValue(petOrderRequestPayload, order_id_jsonpath).toString();
    	String request_petid = jsonUtility.getJsonPathFieldValue(petOrderRequestPayload, order_pet_id_jsonpath).toString();
    	String request_quantity = jsonUtility.getJsonPathFieldValue(petOrderRequestPayload, order_quantity_jsonpath).toString();
    	String request_shipdate = jsonUtility.getJsonPathFieldValue(petOrderRequestPayload, order_shipdate_jsonpath).toString();
    	String request_order_status = jsonUtility.getJsonPathFieldValue(petOrderRequestPayload, order_status_jsonpath).toString();
    	String request_complete = jsonUtility.getJsonPathFieldValue(petOrderRequestPayload, order_complete_jsonpath).toString();
    	
    	
    	// POST call to place an order for pet added in above steps
    	Response petOrderResponse = storeService.storeOrderPet(petOrderRequestPayload);
    	AssertJUnit.assertEquals(petOrderResponse.getStatusCode(), HttpStatus.SC_OK);
    	
    	// GET call to verify purchase order placed in above step
    	Response getResponse = storeService.getStoreOrder(Integer.valueOf(request_order_id));
    	AssertJUnit.assertEquals(getResponse.getStatusCode(), HttpStatus.SC_OK);
    	
    	// Mapping GET call response to POJO
    	StoreOrder storeOrderObject = mapper.readValue(getResponse.getBody().asString(), StoreOrder.class);
    	
    	// Validating details between GET call response & POST call's request payload
    	AssertJUnit.assertEquals(String.valueOf(storeOrderObject.getId()), request_order_id);
    	AssertJUnit.assertEquals(String.valueOf(storeOrderObject.getPetId()), request_petid);
    	AssertJUnit.assertEquals(String.valueOf(storeOrderObject.getQuantity()), request_quantity);
    	AssertJUnit.assertEquals(storeOrderObject.getShipDate().split("T")[0], request_shipdate.split("T")[0]);
    	AssertJUnit.assertEquals(storeOrderObject.getStatus(), request_order_status);
    	AssertJUnit.assertEquals(storeOrderObject.getComplete().toString(), request_complete);
    	
    	
    }
}
