package tests;


import java.io.IOException;
import org.apache.http.HttpStatus;
import org.testng.AssertJUnit;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.helpers.UserService;
import com.model.User;
import com.utils.JsonUtility;
import io.restassured.response.Response;

/*This class file contains test cases related to User API*/
@Listeners(com.utils.ReportUtility.class)
public class UserAPITest {
	
    UserService userService;
    JsonUtility jsonUtility;
    ObjectMapper mapper;
    
    @BeforeClass
    public void init(){
    	userService =new UserService();
    	jsonUtility = new JsonUtility();
    	mapper = new ObjectMapper();
    }
    
	/*
	 * This test case creates a single user through POST call
	 */
    @Test(priority=1)
    public void testCreateUser() throws IOException {
    	
    	String postRequestPayload;
    	
    	// Fetching request payload from testData folder
    	postRequestPayload = jsonUtility.readFileAsString("resources/testData/single_user_payload.json");
    	
    	// POST call to Create User
    	Response response = userService.createUser(postRequestPayload);
    	AssertJUnit.assertEquals(response.getStatusCode(), HttpStatus.SC_OK);
    	
    }
    
	/* This test case creates a multiple users through POST call */
    @Test(priority=2)
    public void testCreateUserWithArray() throws IOException {
    	
    	String postRequestPayload;
    	
    	// Fetching request payload from testData folder
    	postRequestPayload = jsonUtility.readFileAsString("resources/testData/multiple_user_payload.json");
    	
    	// POST call to Create User
    	Response response = userService.createUserWithArray(postRequestPayload);
    	AssertJUnit.assertEquals(response.getStatusCode(), HttpStatus.SC_OK);
    	
    }
    
    /* This test case creates a multiple users through POST call */
    @Test(priority=3)
    public void testCreateUserWithList() throws IOException {
    	
    	String postRequestPayload;
    	
    	// Fetching request payload from testData folder
    	postRequestPayload = jsonUtility.readFileAsString("resources/testData/multiple_user_payload.json");
    	
    	// POST call to Create User
    	Response response = userService.createUserWithList(postRequestPayload);
    	AssertJUnit.assertEquals(response.getStatusCode(), HttpStatus.SC_OK);
    	
    }

	/*
	 * This test case creates a User first through POST call & then verifies the
	 * same user details through GET call
	 */
    @Test(priority=4)
    public void testGetUser() throws IOException {
    	
    	String postRequestPayload;
    	
    	// Fetching request payload from testData folder
    	postRequestPayload = jsonUtility.readFileAsString("resources/testData/single_user_payload.json");
    	
    	// Fetching individual field values from request payload
    	String request_id = jsonUtility.getJsonPathFieldValue(postRequestPayload, "id").toString();
    	String request_username = jsonUtility.getJsonPathFieldValue(postRequestPayload, "username").toString();
    	String request_firstname = jsonUtility.getJsonPathFieldValue(postRequestPayload, "firstName").toString();
    	String request_lastname = jsonUtility.getJsonPathFieldValue(postRequestPayload, "lastName").toString();
    	String request_email = jsonUtility.getJsonPathFieldValue(postRequestPayload, "email").toString();
    	String request_password = jsonUtility.getJsonPathFieldValue(postRequestPayload, "password").toString();
    	String request_phone = jsonUtility.getJsonPathFieldValue(postRequestPayload, "phone").toString();
    	String request_userstatus = jsonUtility.getJsonPathFieldValue(postRequestPayload, "userStatus").toString();
    	
    	// POST call to Create User
    	Response postUserResponse = userService.createUser(postRequestPayload);
    	AssertJUnit.assertEquals(postUserResponse.getStatusCode(), HttpStatus.SC_OK);
    	
    	// GET call to fetch details of User created in above step 
    	Response getUserResponse = userService.getUser(request_username);
    	AssertJUnit.assertEquals(getUserResponse.getStatusCode(), HttpStatus.SC_OK);
    	
    	// Mapping GET call response to POJO
    	User userObject = mapper.readValue(getUserResponse.getBody().asString(), User.class);
    	
    	// Validating the details from request payload against GET call's response
    	AssertJUnit.assertEquals(String.valueOf(userObject.getId()), request_id);
    	AssertJUnit.assertEquals(userObject.getUsername(), request_username);
    	AssertJUnit.assertEquals(userObject.getFirstName(), request_firstname);
    	AssertJUnit.assertEquals(userObject.getLastName(), request_lastname);
    	AssertJUnit.assertEquals(userObject.getEmail(), request_email);
    	AssertJUnit.assertEquals(userObject.getPassword(), request_password);
    	AssertJUnit.assertEquals(userObject.getPhone(), request_phone);
    	AssertJUnit.assertEquals(String.valueOf(userObject.getUserStatus()), request_userstatus);
    }
}
