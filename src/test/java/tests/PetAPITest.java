package tests;


import java.io.IOException;
import org.apache.http.HttpStatus;
import org.testng.AssertJUnit;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.helpers.PetService;
import com.model.Pet;
import com.utils.ConfigManager;
import com.utils.JsonUtility;
import io.restassured.response.Response;

/*This class file contains test cases related to Pet API*/
@Listeners(com.utils.ReportUtility.class)
public class PetAPITest  {
	
    PetService petService;
    JsonUtility jsonUtility;
    ObjectMapper mapper;
   
    
    @BeforeClass
    public void init(){
    	petService =new PetService();
    	jsonUtility = new JsonUtility();
    	mapper = new ObjectMapper();
    }
    
	/*
	 * This test case performs POST call to add a pet & then verifies pet details
	 * through GET call
	 */
	
    @Test(priority=1)
    public void addPetAndVerifyDetails() throws IOException {
    	
    	String postRequestPayload;
    	
    	// Fetching jsonpath from properties file
    	String pet_id_jsonpath=ConfigManager.getInstance().getString("pet_id");
    	String category_id_jsonpath=ConfigManager.getInstance().getString("category_id");
    	String category_name_jsonpath=ConfigManager.getInstance().getString("category_name");
    	String pet_name_jsonpath=ConfigManager.getInstance().getString("pet_name");
    	String pet_status_jsonpath=ConfigManager.getInstance().getString("pet_status");
    	String pet_tag_id_jsonpath=ConfigManager.getInstance().getString("pet_tag_id");
    	String pet_tag_name_jsonpath=ConfigManager.getInstance().getString("pet_tag_name");
    	
    	// Fetching request payload from testData folder
    	postRequestPayload = jsonUtility.readFileAsString("resources/testData/pet_payload.json");
    	
    	// Fetching individual field values from request payload
    	String request_id = jsonUtility.getJsonPathFieldValue(postRequestPayload, pet_id_jsonpath).toString();
    	String request_categoryid = jsonUtility.getJsonPathFieldValue(postRequestPayload, category_id_jsonpath).toString();
    	String request_categoryname = jsonUtility.getJsonPathFieldValue(postRequestPayload, category_name_jsonpath).toString();
    	String request_name = jsonUtility.getJsonPathFieldValue(postRequestPayload, pet_name_jsonpath).toString();
    	String request_status = jsonUtility.getJsonPathFieldValue(postRequestPayload, pet_status_jsonpath).toString();
    	String request_tagid = jsonUtility.getJsonPathFieldValue(postRequestPayload, pet_tag_id_jsonpath).toString();
    	String request_tagname = jsonUtility.getJsonPathFieldValue(postRequestPayload, pet_tag_name_jsonpath).toString();
    	
    	// POST call to add a pet to the store
    	Response postResponse = petService.addPetToStore(postRequestPayload);
    	AssertJUnit.assertEquals(postResponse.getStatusCode(), HttpStatus.SC_OK);
    	
    	// GET call to fetch details of Pet added in above step
    	Response getResponse = petService.getPetById(Integer.valueOf(request_id));
    	AssertJUnit.assertEquals(getResponse.getStatusCode(), HttpStatus.SC_OK);
    	
    	// Mapping GET call response to POJO
    	Pet petObject = mapper.readValue(getResponse.getBody().asString(), Pet.class);
    	
    	// Validating details between GET call response & POST call's request payload
    	AssertJUnit.assertEquals(String.valueOf(petObject.getId()), request_id);
    	AssertJUnit.assertEquals(String.valueOf(petObject.getCategory().getId()), request_categoryid);
    	AssertJUnit.assertEquals(petObject.getCategory().getName(), request_categoryname);
    	AssertJUnit.assertEquals(petObject.getName(), request_name);
    	AssertJUnit.assertEquals(petObject.getStatus(), request_status);
    	AssertJUnit.assertEquals(String.valueOf(petObject.getTags().get(0).getId()), request_tagid);
    	AssertJUnit.assertEquals(petObject.getTags().get(0).getName(), request_tagname);
    	
    }
}
