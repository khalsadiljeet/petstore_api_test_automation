package com.helpers;

import static io.restassured.RestAssured.given;
import com.constants.EndPoints;
import com.utils.ConfigManager;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

/*This class has reusable methods related to operations 
applicable for Store API*/

public class StoreService {
   
    private static final String BASE_URL = ConfigManager.getInstance().getString("base_url");
    private static final String header_value = ConfigManager.getInstance().getString("accept_header_value");
    private static final String api_key_value = ConfigManager.getInstance().getString("api_key_header_value");

    public StoreService(){
        RestAssured.baseURI=BASE_URL;
    }

    // POST call to place an order for Pet
    public Response storeOrderPet(String payload){
        
        Response response = given()
                .header("Accept", header_value)
                .header("api_key", api_key_value)
                .contentType(ContentType.JSON)
                .body(payload)
                .post(EndPoints.store_post)
                .then().extract().response();

        return response;
    }
    
    // GET call to find purchase order by ID
    public Response getStoreOrder(int orderId){
        
        Response response = given()
                .header("Accept", header_value)
                .header("api_key", api_key_value)
                .contentType(ContentType.JSON)
                .pathParam("orderId", orderId)
                .get(EndPoints.store_order_get)
                .then().extract().response();

        return response;
    }
    
    // GET call to fetch pet inventories by status
    public Response getStoreInventory(){
        
        Response response = given()
                .header("Accept", header_value)
                .header("api_key", api_key_value)
                .contentType(ContentType.JSON)
                .get(EndPoints.store_inventory_get)
                .then().extract().response();

        return response;
    }
    
    // DELETE call to delete purchase order
    public Response deleteStoreOrder(int orderId){
       
	   Response response = given()
               .header("Accept", header_value)
               .header("api_key", api_key_value)
               .contentType(ContentType.JSON)
               .pathParam("username", orderId)
               .delete(EndPoints.store_order_delete)
               .then().extract().response();

        return response;
    }
}
