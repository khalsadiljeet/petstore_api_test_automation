package com.helpers;

import static io.restassured.RestAssured.given;
import com.constants.EndPoints;
import com.utils.ConfigManager;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;


/*This class has reusable methods related to operations 
applicable for User API*/

public class UserService {
    
    private static final String BASE_URL = ConfigManager.getInstance().getString("base_url");
    private static final String header_value = ConfigManager.getInstance().getString("accept_header_value");
    private static final String api_key_value = ConfigManager.getInstance().getString("api_key_header_value");
    
    public UserService(){
        RestAssured.baseURI=BASE_URL;
    }

    // POST call to create single user
    public Response createUser(String payload){
        
        Response response = given()
                .header("Accept", header_value)
                .header("api_key", api_key_value)
                .contentType(ContentType.JSON)
                .body(payload)
                .post(EndPoints.user_post_create)
                .then().extract().response();

        return response;
    }
    
    // POST call to create multiple user
    public Response createUserWithArray(String payload){
        
        Response response = given()
                .header("Accept", header_value)
                .header("api_key", api_key_value)
                .contentType(ContentType.JSON)
                .body(payload)
                .post(EndPoints.user_post_create_with_array)
                .then().extract().response();

        return response;
    }

    // POST call to create multiple user
    public Response createUserWithList(String payload){
    
         Response response = given()
            .header("Accept", header_value)
            .header("api_key", api_key_value)
            .contentType(ContentType.JSON)
            .body(payload)
            .post(EndPoints.user_post_create_with_list)
            .then().extract().response();

         return response;
    }
    
    // GET call to fetch details of the User
    public Response getUser(String username){
        
        Response response = given()
                .header("Accept", header_value)
                .header("api_key", api_key_value)
                .contentType(ContentType.JSON)
                .pathParam("username", username)
                .get(EndPoints.user_get)
                .then().extract().response();

        return response;
    }
    
   // DELETE call to delete the User
   public Response deleteUser(String username){
        
	   Response response = given()
               .header("Accept", header_value)
               .header("api_key", api_key_value)
               .contentType(ContentType.JSON)
               .pathParam("username", username)
               .delete(EndPoints.user_delete)
               .then().extract().response();

        return response;
    }
   
   // PUT call to update User details
   public Response updateUser(String username){
       
	   Response response = given()
               .header("Accept", header_value)
               .header("api_key", api_key_value)
               .contentType(ContentType.JSON)
               .pathParam("username", username)
               .put(EndPoints.user_put)
               .then().extract().response();

        return response;
    }
   
   // GET call to log user into the system
   public Response getUserLogin(String username, String password){
       
	   Response response = given()
               .header("Accept", header_value)
               .header("api_key", api_key_value)
               .contentType(ContentType.JSON)
               .queryParam("per-page", username)
               .queryParam("page", password)
               .get(EndPoints.user_login_get)
               .then().extract().response();
	   
        return response;
    }
   
    // GET call to log out current user from the system
    public Response getUserLogout(String username, String password){
       
	   Response response = given()
               .header("Accept", header_value)
               .header("api_key", api_key_value)
               .contentType(ContentType.JSON)
               .queryParam("per-page", username)
               .queryParam("page", password)
               .get(EndPoints.user_logout_get)
               .then().extract().response();
	   
        return response;
    }
   
}
