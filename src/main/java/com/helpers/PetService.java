package com.helpers;

import static io.restassured.RestAssured.given;
import com.constants.EndPoints;
import com.utils.ConfigManager;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

/*This class has reusable methods related to operations 
applicable for Pet API*/

public class PetService {
   
    private static final String BASE_URL = ConfigManager.getInstance().getString("base_url");
    private static final String api_key_value = ConfigManager.getInstance().getString("api_key_header_value");
    private static final String header_value = ConfigManager.getInstance().getString("accept_header_value");

    public PetService(){
        RestAssured.baseURI=BASE_URL;
    }

    // POST call to add a new Pet to the store
    public Response addPetToStore(String payload){
        
        Response response = given()
        		.header("Accept", header_value)
                .header("api_key", api_key_value)
                .contentType(ContentType.JSON)
                .body(payload)
                .post(EndPoints.pet_post)
                .then().extract().response();

        return response;
    }
    
    // POST call to upload an image for a Pet
    public Response uploadPetImage(String payload){
        
        Response response = given()
        		.header("Accept", header_value)
                .header("api_key", api_key_value)
                .contentType(ContentType.JSON)
                .body(payload)
                .post(EndPoints.pet_post_upload_image)
                .then().extract().response();

        return response;
    }
    
    // GET call to fetch details of pet based on ID
    public Response getPetById(int petId){
        
        Response response = given()
                .header("Accept", header_value)
                .header("api_key", api_key_value)
                .contentType(ContentType.JSON)
                .pathParam("petId", petId)
                .get(EndPoints.pet_get_by_id)
                .then().extract().response();

        return response;
    }
    
    // GET call to fetch details of pet based on status
    public Response getPetByStatus(){
        
        Response response = given()
                .header("Accept", header_value)
                .header("api_key", api_key_value)
                .contentType(ContentType.JSON)
                .get(EndPoints.pet_get_find_by_status)
                .then().extract().response();

        return response;
    }
    
    // DELETE call to delete pet based on ID
    public Response deletePet(int petId){
        
 	   Response response = given()
                .header("Accept", header_value)
                .header("api_key", api_key_value)
                .contentType(ContentType.JSON)
                .pathParam("petId", petId)
                .delete(EndPoints.store_order_delete)
                .then().extract().response();

         return response;
     }
    
    // PUT call to update pet details
    public Response updatePet(String payload){
        
 	   Response response = given()
                .header("Accept", header_value)
                .header("api_key", api_key_value)
                .contentType(ContentType.JSON)
                .put(EndPoints.pet_put)
                .then().extract().response();

         return response;
     }
    
}
