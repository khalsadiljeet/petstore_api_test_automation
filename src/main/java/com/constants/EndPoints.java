package com.constants;

/*This class contains all the endpoints related to 
User, Pet & Store APIs*/

public class EndPoints {
	
	//User API endpoints
    public static final String user_get = "/user/{username}";
    public static final String user_login_get = "/user/login";
    public static final String user_logout_get = "/user/logout";
    public static final String user_post_create = "/user";
    public static final String user_post_create_with_array = "/user/createWithArray";
    public static final String user_post_create_with_list = "/user/createWithList";
    public static final String user_put = "/user/{username}";
    public static final String user_delete = "/user/{username}";
    
    //Store API endpoints
    public static final String store_post = "/store/order";
    public static final String store_order_get = "/store/order/{orderId}";
    public static final String store_inventory_get = "/store/inventory";
    public static final String store_order_delete = "/store/order/{orderId}";
    
    //Pet API endpoints
    public static final String pet_get_by_id = "/pet/{petId}";
    public static final String pet_get_find_by_status = "/pet/findByStatus";
    public static final String pet_post = "/pet";
    public static final String pet_post_upload_image = "/pet/{petId}/uploadImage";
    public static final String pet_post_update = "/pet/{petId}";
    public static final String pet_put = "/pet";
    public static final String pet_delete = "/pet/{petId}";
    
}
