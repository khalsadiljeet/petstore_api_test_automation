Given:
1. Environment and specs:
   https://petstore.swagger.io/ (API)
2. Scripting language used: Java
3. Testing tool: Eclipse
4. CI platform: Gitlab

Scenarios:
 1. User is able to place an order for a Pet through POST call & then verify same purchase order details through GET
    call at Store.   
 2. User is able to add a Pet through POST call & then verify same details through GET call.
 3. Create a User through POST call & then verify details of the User through GET call.
 4. Create multiple users at once.
 5. End to End Test case: Able to add Pet into the system, placing the order for the same Pet & verifying purchase order details.

a. How to run test cases locally?
 1. testng.xml file -> Right click on testng.xml file & then Run As -> TestNG Suite. This will run all the test cases
    at once
 2. Click on Run button available just above individual test methods
 
b. How to run the test in a CI/CD pipeline?
   1. Navigate to repository on gitlab.com "https://gitlab.com/khalsadiljeet/petstore_api_test_automation", click CI/CD -> Pipelines
      & then click Run Pipeline button. This will run the tests in pipeline.
   2. On every change (commit & push) to repository, automatic pipeline will be triggered which will run tests.
   
c. Link to results in Calliope.pro
   https://app.calliope.pro/reports/147040
   
d. Describe one improvement point and one new feature for the Calliope.pro platform                                                                                 
   Improvement point: Looks like there is no support for html reports for now, supporting html reports as well can be a improvement
   point.                                                                                                                          
   
   New feature Suggestion: There can be a feature which allows team members to post comment(s) onto the Reports. These 
   comments could be used for providing any analysis or tagging someone from team to draw their attention or for any failed
   test case if someone knows there is already a defect for it then defect id can be mentioned in the comments etc.
   
e. What you used to select the scenarios, what was your approach?                                
   When choosing scenarios to automate, its good to automate repeatable & common scenarios, also in this case approach was to 
   cover atleast 1 scenario from each API. Tried covering one end to end test case as well.

f. Why are they the most important?                                            
   Looking application under test, i feel below are the most common user cases:
   1. addPetAndVerifyDetails() -> As system/application is related to Pet Store, most important thing would be able to add 
      pet into the system with it's relevant details & verify whether those details are present in the system.
   2. placePetOrderAndVerifyPurchase() -> User should be able to place an order for pets & verify purchase order details.
   3. testCreateUser()/testGetUser() -> Able to create User(s) & fetch details of those User(s).
   4. End to End -> addPetAndPlaceItsOrder() -> Able to add Pet into the system, placing the order for the same Pet & 
      verifying purchase order details.
   
g. What could be the next steps to your project?                                 
   According to Testing pyramid, evaluate which scenarios can be part of API/UI, then cover as much scenarios to increase 
   the coverage including negative scenarios:
   1. Cover remaining scenarios (remaining operation calls from User/Pet/Store APIs including 
   negative scenarios).
   2. Currently, static test data is used in the framework, we can have json schemas added & through code we can dynamically create
      test data & pass to a request.
   3. Reporting can be improved like which has more details. Serenity report is such report where we can show Request/Response 
      payloads, details of each API operation. Also, we can try sending report over email after every execution.
   3. In case regression suite gets heavy in future, we can provide support for parallel execution of test cases to reduce the
      overall execution time.
   4. Implement loggers.
